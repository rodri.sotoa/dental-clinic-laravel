@extends('welcome')
@section('navbar')
@parent
@endsection
@section('content')
<h1 class="text-center">Agendar Consulta Dental</h1>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="card ">
    <form action="{{route('appointments.store')}}" method="post">
        @csrf
        <div class="card-body">
            <br>
            <div class="row form-group">
                <label for="service" class="col-md-2">Servicio </label>
                <select name="service_id" id="service" class="form-control col-md-6">
                    <option>-</option>
                    @foreach($services as $service)
                        <option value="{{$service->id}}">{{$service->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="row form-group">
                <label for="dentist" class="col-md-2">Médico Tratante </label>
                <select name="dentist_id" id="dentist" class="form-control col-md-6">
                    <option>-</option>
                    @foreach($dentists as $dentist)
                        <option value="{{$dentist->id}}">{{$dentist->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="row form-group">
                <label for="patient" class="col-md-2">Nombre Paciente </label>
                <select name="patient_id" id="patient" class="form-control col-md-6">
                    <option>-</option>
                    @foreach($patients as $patient)
                        <option value="{{$patient->id}}">{{$patient->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="row form-group">
                <label for="price" class="col-md-2">Costo Servicio </label>
                <div class="col-md-6" style="padding-left: 0;padding-right: 0;">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">$</span>
                        </div>
                        <input type="number" name="price" id="price" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <label for="date" class="col-md-2">Fecha Servicio </label>
                <input type="date" name="date" id="date" class="form-control col-md-6">
            </div>
            <div class="row">
                <button type="submit" class="btn btn-primary col-md-4 offset-2">Agendar Consulta</button>
            </div>
        </div>
    </form>
</div>
@endsection
@section('custom')
<script type="text/javascript">
            function val(value){
                $('#price').val(value);
            }
            $('#service').change(function(){
                val(value);
            });

        
        //console.log(costo);
</script>
@endsection