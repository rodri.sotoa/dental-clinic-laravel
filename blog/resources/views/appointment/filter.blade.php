@extends('welcome')
@section('navbar')
@parent
@endsection
@section('content')
<h1 class="text-center">Historial de Servicios</h1>
    <br>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="container">
        
        <form action="{{route('appointments.filter')}}" method="get">
                @csrf
            <div class="row form-group">
                <label for="datei" class="col-md-1">Desde: </label>
                <div class="col-md-4">
                    <input type="date" name="datei" id="datei" class="form-control col-md-12">
                </div>
                <label for="datef" class="col-md-1">Hasta: </label>
                <div class="col-md-4">
                    <input type="date" name="datef" id="datef" class="form-control col-md-12">
                </div>
                <div class="col-md-2">
                    <button type="submit" class="btn btn-primary">Filtrar</button>
                </div>
            </div>
        </form>
        <table class="table table-striped table-bordered" id="table">
            <thead>
                <th>Fecha Consulta</th>
                <th class="no-sort">Paciente</th>
                <th class="no-sort">Servicio</th>
                <th class="no-sort">Medico</th>
                <th class="no-sort">Precio Servicio</th>
                <th class="no-sort">Opciones</th>
            </thead>
            <tbody>
                @foreach($appointments as $appointment)
                <tr>
                    <td>{{$appointment->date}}</td>
                    <td>{{$appointment->patient->name}}</td>
                    <td>{{$appointment->service->name}}</td>
                    <td>{{$appointment->dentist->name}}</td>
                    <td>$ {{number_format($appointment->price,0,",",".")}}</td>
                    <td>
                        <form action="{{ route('appointments.destroy',$appointment->id) }}" method="post">
                            
                            
                            <a href="{{ route('appointments.edit',$appointment->id) }}" class="btn "><span><i class="fas fa-pencil-alt fa-sm"></i></span></a>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn "><span><i class="fas fa-trash-alt fa-sm"></i></span></button>
                            
                        </form>
                        
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <br>
        <h2>Ganancia Total por Periodo: $ {{number_format($profit,0,",",".")}} .-</h2>
    </div>
@endsection
@section('custom')
    
@endsection