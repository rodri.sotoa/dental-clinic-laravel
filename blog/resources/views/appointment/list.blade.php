@extends('welcome')
@section('navbar')
@parent
@endsection
@section('content')
<h1 class="text-center">Historial de Servicios</h1>
    <br>
    <div class="container">
        <table class="table table-striped table-bordered" id="table">
            <thead>
                <th>Fecha Consulta</th>
                <th class="no-sort">Paciente</th>
                <th class="no-sort">Servicio</th>
                <th class="no-sort">Medico</th>
                <th class="no-sort">Precio Servicio</th>
                <th class="no-sort">Opciones</th>
            </thead>
            <tbody>
                @foreach($appointments as $appointment)
                <tr>
                    <td>{{$appointment->date}}</td>
                    <td>{{$appointment->patient->name}}</td>
                    <td>{{$appointment->service->name}}</td>
                    <td>{{$appointment->dentist->name}}</td>
                    <td>$ {{number_format($appointment->price,0,",",".")}}</td>
                    <td>
                        <form action="{{ route('appointments.destroy',$appointment->id) }}" method="post">
                            
                            
                            <a href="{{ route('appointments.edit',$appointment->id) }}" class="btn "><span><i class="fas fa-pencil-alt fa-sm"></i></span></a>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn "><span><i class="fas fa-trash-alt fa-sm"></i></span></button>
                            
                        </form>
                        
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <br>
        <h2>Ganancia Total: $ {{number_format($profit,0,",",".")}} .-</h2>
    </div>
@endsection
@section('custom')
    <script type="text/javascript">
        $(document).ready( function () {
            $('#table').DataTable({
                "paging":   false,
                "ordering": true,
                "info":     false,
                "searching": false,
                columnDefs: [{
                  orderable: false,
                  targets: "no-sort"
                }]
            });
        });
    </script>
@endsection