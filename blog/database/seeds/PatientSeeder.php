<?php

use Illuminate\Database\Seeder;

class PatientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('patients')->insert([
            'name' => "Susana Ramirez",
        ]);
        DB::table('patients')->insert([
        	'name' => "Hector Troncoso",
        ]);
        DB::table('patients')->insert([
            'name' => "Marco Palma",
        ]);
        DB::table('patients')->insert([
            'name' => "Armando Barra",
        ]);
        DB::table('patients')->insert([
            'name' => "Maria Toro",
        ]);
    }
}
