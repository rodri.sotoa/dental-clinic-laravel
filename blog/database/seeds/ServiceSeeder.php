<?php

use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('services')->insert([
            'name' => "Rayos X",
            'price' => 30750,
        ]);
        DB::table('services')->insert([
        	'name' => "Limpieza Dental",
            'price' => 55000,
        ]);
        DB::table('services')->insert([
            'name' => "Ortodoncia",
            'price' => 250000,
        ]);
        DB::table('services')->insert([
            'name' => "Restauracion Compuesta",
            'price' => 585000,
        ]);
    }
}
