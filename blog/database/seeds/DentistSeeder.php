<?php

use Illuminate\Database\Seeder;

class DentistSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('dentists')->insert([
            'name' => "Nick Riviera",
        ]);
        DB::table('dentists')->insert([
        	'name' => "César Cisternas",
            
        ]);
        DB::table('dentists')->insert([
            'name' => "Timoteo Alegria",
            
        ]);
        DB::table('dentists')->insert([
            'name' => "Hernan Hacienda",
            
        ]);
    }
}
