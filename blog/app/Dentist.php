<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dentist extends Model
{
    //
    protected $table = "dentists";
}
