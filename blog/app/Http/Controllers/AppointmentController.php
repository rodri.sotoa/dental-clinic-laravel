<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Dentist;
use App\Service;
use App\Patient;
use Illuminate\Http\Request;

class AppointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $appointments = Appointment::all();
        $profit = 0;
        $appointment_total = 0;
        $service_total = 0;
        foreach ($appointments as $appointment => $a) {
            $appointment_total = $appointment_total + $a->price;
            $service_total = $service_total + $a->service->price;
        }
        $profit = $appointment_total - $service_total;
        return view('appointment.list',compact('appointments','profit'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function filter(Request $request)
    {
        if($request != null){
            $appointments = Appointment::all()->where('date','>=',$request->input('datei'))
            ->where('date','<=',$request->input('datef'));
            $profit = 0;
            $appointment_total = 0;
            $service_total = 0;
            foreach ($appointments as $appointment => $a) {
                $appointment_total = $appointment_total + $a->price;
                $service_total = $service_total + $a->service->price;
            }
            $profit = $appointment_total - $service_total;
            return view('appointment.filter',compact('appointments','profit'));

        }else{
            $appointments = Appointment::all();
            return view('appointment.filter',compact('appointments'));
        }

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $dentists = Dentist::all();
        $services = Service::all();
        $patients = Patient::all();
        return view('appointment.create',compact('services','dentists','patients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'date' => 'required',
            'price' => 'required',
            'dentist_id' => 'required|integer',
            'patient_id' => 'required|integer',
            'service_id' => 'required|integer',
        ]);

        Appointment::create($request->all());
        return redirect()->route('appointments.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function show(Appointment $appointment)
    {
        //
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function edit(Appointment $appointment)
    {
        //
        $dentists = Dentist::all();
        $services = Service::all();
        $patients = Patient::all();
        return view('appointment.edit',compact('appointment','dentists','services','patients'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Appointment $appointment)
    {
        //
        $request->validate([
            'date' => 'required',
            'price' => 'required',
            'dentist_id' => 'required|integer',
            'patient_id' => 'required|integer',
            'service_id' => 'required|integer',
        ]);

        $appointment->update($request->all());
  
        return redirect()->route('appointments.index')->with('exito','Consulta modificada correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Appointment $appointment)
    {
        //
        
        $appointment->delete();
        return redirect()->route('appointments.index')->with('exito','Consulta eliminada correctamente');
    }
}
