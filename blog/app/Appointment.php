<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    //
    protected $table = "appointments";
    protected $fillable = ['date', 'price', 'dentist_id','patient_id','service_id'];

    public function dentist(){
    	return $this->belongsTo('App\Dentist');
    }

    public function patient(){
    	return $this->belongsTo('App\Patient');
    }

    public function service(){
    	return $this->belongsTo('App\Service');
    }
}
